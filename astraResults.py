from scipy.io.netcdf import netcdf_file
import numpy as np
import matplotlib.pyplot as plt 

class astraResults:

    def __init__(self):
        self.ne = None
        self.te = None
        self.ti = None
        self.nz = None
        self.rho = None

    def loadResults(self,ncdfOutputFile):

        # load from netcdf file  
        astra_dat = netcdf_file(ncdfOutputFile, mmap=False).variables
        self.ne = astra_dat['NE'].data[-1]
        self.te = astra_dat['TE'].data[-1]
        self.ti = astra_dat['TI'].data[-1]
        self.nz = astra_dat['NIZ1'].data[-1]
        self.rho = astra_dat['XRHO'].data 

    def fitResults(self, parameter, plot=None): # parameter dovrebbe essere un ne/te/ni/nimp

        self.coeff = None
        self.astraResultVector = None
        self.grade = None

        grade = 7 #Note that we may need a pre-processing of the vector to determine the optimal grade of the fitting polynomial 
        self.grade = grade 

        if parameter == 'ne':
            astraResultVector = self.ne
        '''
        elif parameter == 'te':
            astraResultVector = self.te
        elif parameter == 'ti':
            astraResultVector = self.ti
        elif parameter == 'nimp':
            astraResultVector = self.nz
        '''
          
        coeff = np.polyfit(self.rho, astraResultVector, grade)
        self.coeff = coeff

        if plot is not None:
            # plot vector against its polynomial approximation to check that it was correctly implemented 
            pol = np.poly1d(self.coeff)
            y_fit = pol(self.rho)
            
            plt.plot(self.rho, y_fit, label = 'Fit')
            plt.plot(self.rho, astraResultVector, ls = '--', label = 'Initial')
            plt.title("Ne") # !!! NOTA qui devi fare in modo che il nome del grafico vari in base al parametro  
            plt.legend()
            plt.show()
            # ... 

        #return coeff # o i c oefficienti?  
