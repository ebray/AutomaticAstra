from astraResults import astraResults
import fileinput
import os
import subprocess
import numpy as np

    # ---
    # preliminary operations a_mano:
    # run TRVIEW
    # create fml file for NIMP and NEFIT, TEFIT, TIFIT 
    # ---

    # Select shotfile number
    #
    # Select base folder
    # 
    # Select equ file
    # 
    # Modify equ file: (IF it==0, fix NEX,TEX ELSE fix NEFIT, TEFIT) and let NIZ evolve
    # 
    # launch ASTRA simulation to provide updated impurity profile
    # 
    # retrieve computed impurity profile
    # 
    # fit imp profile with analytical expression
    # 
    # assign the expression to NIMP
    # 
    # copy equ file to a new one called <baseEquFileName>_it<nit>_pls 
    # 
    # modify the new equ to evolve TE,NE with fixed NIZ = NIMP
    # 
    # launch ASTRA simulation to provide updated NE, TE
    #
    # retrieve computed plasma profiles
    # 
    # fit profiles with analytical expression
    #  
    # assign the expressionsby modifying the formulas for NEFIT, TEFIT, NIFIT
    # 
    # plot for comparison with exp data (both fit and resulting vector)

def main():

    # Select shotfile number

    shotfile = 41279

    # Select base folder

    baseDir = '/shares/departments/AUG/users/ebray'

    # Select equ file

    equFileDir = 'a8/equ'

    equFileName = 'IterationTest'
    fmlFileName = ['nefit', 'tefit', 'tifit', 'nimp']

    ncdfOutputDir = baseDir+'/'+'a8/MyOutput/'
    ncdfOutputFileName = 'BckFixedImp1_5.CDF' # NOTA: questo file andrebbe aggiornato alla fine di ogni ciclo  

    iter = 1 ## !!!!! THIS HAS TO BE SET AS 0 !!!!! ##    

    # The first equ has the bck plasma set and the evolution of nz.  

    ## here goes for loop for iterations 

    # Modify equ file to fix background plasma and let nz evolve - at the first iteration TEFIT=TEX and then it will be updated

    if iter > 0:
        # define NEFIT, TEFIT, TIFIT, NIMP 
        
        # apri file fml/nefit 
        # FIXME FIXME FIXME ora usa file di prova, poi modifica con quello giusto e il path completo
        res = astraResults()  
        res.loadResults(ncdfOutputFileName)

        for element in fmlFileName:
            if element == 'nefit': 
                
                # trovo i coefficienti del polyfit 
                res.fitResults('ne', plot=True)
                coeffNE = res.coeff    
                grade = res.grade                
                # for line in fileinput.input(baseDir+'/'+equFileDir+'/'+fmlFileName,inplace=True)
                for line in fileinput.input('nefit', inplace=True):
                    # sostiusci i coefficienti 
                    if 'Y = ' in line: 

                        polynomial = ' '.join(('{}*Z**{}'.format(f'+{coeffNE[i]}' if coeffNE[i] >= 0 else coeffNE[i], 7 - i)) for i in np.arange(7))
                        last_term = f'+{coeffNE[7]}' if coeffNE[7] >= 0 else str(coeffNE[7])
                        full_expr = polynomial + ' ' + last_term
                        print('Y = {}'.format(full_expr))

                    else:
                        print('{}'.format(line), end='')
                        
        # fix  NEFIT, TEFIT, TIFIT and evolve F1 
        # FIXME FIXME FIXME per ora leggo e modifico il file equ in questa cartella, poi dovro modificare quello giusto unendo i path  
        #for line in fileinput.input(baseDir+'/'+equFileDir+'/'+equFileName,inplace=True)
        
        for line in fileinput.input('IterationTest', inplace=True):
            if 'NE=N' in line:
                print('NE=NEFIT;', end='\n')
            elif 'NE:' in line:
                print('NE:EQ[2,CF16];', end='\n') #continuare #NOTAA: togliere righe commentate 
            elif 'NEB=N' in line:
                print('NEB=NEFIT(AFX(CF16));', end='\n')
            elif 'TE=T' in line:
                print('TE=TEFIT;', end='\n')            
            elif 'TE:' in line:
                print('TE:EQ[2,CF16];', end='\n')
            elif 'TEB=T' in line:
                print('TEB=TEFIT(AFX(CF16));', end='\n')
            elif 'TI=T' in line:
                print('TI=TIFIT;', end='\n')
            elif 'TI:' in line:
                print('TI:EQ[2,CF16];', end='\n')
            elif 'TIB=T' in line:
                print('TIB=TIFIT(AFX(CF16));', end='\n')                
            elif 'F1 = N' in line:
                print('F1 = NIMP;', end='\n')
            elif 'F1:' in line:
                print('F1:AS;', end='\n')
            elif 'F1B = N' in line:
                print('F1B = NIMP(AFX(CF16));', end='\n')
            else:
                print('{}'.format(line), end='')
        
    # launch ASTRA simulation to provide updated impurity profile

    bashCommand = baseDir+'/a8/exe/as_exe -m ' +baseDir+'/'+equFileDir+'/'+equFileName+ ' -v aug41279 -dev aug -s 0. -e 15. -tpause 15.'

    process = subprocess.Popen(bashCommand.split(), stdout = subprocess.PIPE)

    # NOTA!! Non credo mi serva avere il file di output, al massimo quello di errore  
    output, error = process.communicate()

    # retrieve computed impurity profile
    # 
    # fit imp profile with analytical expression
    # 
    # assign the expression to NIMP

    res = astraResults
    res.loadResults(res,ncdfOutputFile)
    nz = res.nz

if __name__ == "__main__":
    main()


